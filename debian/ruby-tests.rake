require 'gem2deb/rake/testtask'
# Disable tests that require 'x25519'.
# Unfortunately this also disables 'ed25519' tests.
ENV['NET_SSH_NO_ED25519'] = '1'
ENV.delete("CI")

# Some tests rely no ciphers which are considered legacy in OpenSSL 3. For now,
# let's use the custom config file to enable them and make the tests pass.
require 'openssl'
if OpenSSL::OPENSSL_LIBRARY_VERSION.start_with? "OpenSSL 3"
  ENV['OPENSSL_CONF'] = Dir.pwd + '/test/openssl3.conf'
end

Gem2Deb::Rake::TestTask.new do |t|
end
